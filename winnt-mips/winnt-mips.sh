#!/bin/bash

qemu-system-mips64el \
    -machine magnum \
    -cpu R4000 \
    -m 128M \
    -smp cores=1,sockets=1 \
    -bios "firmware/NTPROM.RAW" \
    -global ds1225y.filename="firmware/NVRAM.RAW" \
    -netdev user,id=net0 \
    -net nic,netdev=net0,id=nic0,macaddr=52:54:00:3c:0d:39 \
    -drive id=winnt,if=scsi,format=qcow2,file="winnt-mips.qcow2" \
    $@
