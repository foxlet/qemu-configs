#!/bin/bash

qemu-system-i386 \
    -machine pc,accel=tcg \
    -cpu pentium3 \
    -m 512M \
    -smp cores=1,sockets=1 \
    -device cirrus-vga,vgamem_mb=16 \
    -netdev user,id=net0 \
    -device ne2k_pci,netdev=net0,id=nic0,mac=52:54:00:3f:e9:2e \
    -drive format=qcow2,cache=none,if=ide,id=sda,file="win9x.qcow2" \
    $@
