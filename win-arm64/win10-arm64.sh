#!/bin/bash

qemu-system-aarch64 \
    -machine virt,virtualization=true,highmem=false \
    -cpu cortex-a53 \
    -m 3G \
    -smp cores=6,sockets=1 \
    -drive if=pflash,format=raw,readonly,file="firmware/QEMU_EFI.fd" \
    -drive if=pflash,format=raw,file="firmware/QEMU_VARS.fd" \
    -device VGA \
    -nic none \
    -device qemu-xhci -device usb-kbd -device usb-tablet \
    -drive file="virtio-win.iso",if=none,media=cdrom,id=drivers \
    -device usb-storage,drive=drivers \
    -drive file="PROFESSIONAL_ARM64_EN-US.ISO",if=none,media=cdrom,id=setup \
    -device usb-storage,drive=setup \
    -drive file="win10-arm64.qcow2",if=virtio,cache=none,aio=threads \
    $@
